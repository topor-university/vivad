import base64

from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser, PermissionsMixin)
from django.db import models

class CustomUserManager(BaseUserManager):
    def create_user(self, username, password, email, favorite_phrase):
        if not username:
            raise ValueError('Users must have an username')

        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
                username=username,
                email=self.normalize_email(email),
                favorite_phrase=favorite_phrase
            )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, email=None, favorite_phrase=None):
        if not email:
            email = 'admin@example.com'

        if not favorite_phrase:
            favorite_phrase = ''

        user = self.create_user(username, password, email, favorite_phrase)

        user.is_admin = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=128, unique=True)
    email = models.EmailField(max_length=128)
    favorite_phrase = models.CharField(max_length=128)
    secrets = models.TextField(default='[]')

    # is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = ['email']
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def is_superuser(self):
        return self.is_admin

    @property
    def favorite_phrase_base64(self):
        return base64.encodebytes(self.favorite_phrase.encode('utf-8')).decode('utf-8')
