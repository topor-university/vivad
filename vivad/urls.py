from django.urls import include, path

from . import views
from . import ajax_urls

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('storage/', views.IndexFrameView.as_view(), name='main-page'),
    path('storage/', views.IndexFrameView.as_view(), name='handle-directory-form'),
    path('storage/', views.IndexFrameView.as_view(), name='handle-secret-form'),
    path('storage/', views.IndexFrameView.as_view(), name='handle-show-site-form'),
    path('login/', views.LoginView.as_view(), name='login'),
    path('logout/', views.logoutView, name='logout'),
    path('registration/', views.RegistrationView.as_view(), name='registration'),
    path('settings/', views.SettingsView.as_view(), name='settings'),
    path('settings/edit-password/', views.SettingsEditPasswordView.as_view(), name='handle-settings-edit-password'),
    path('settings/edit-favorite-phrase/', views.SettingsEditFavoritePhraseView.as_view(), name='handle-settings-edit-favorite-phrase'),
    path('settings/delete-account/', views.SettingsDeleteAccountView.as_view(), name='handle-settings-delete-account'),
    path('ajax/', include(ajax_urls))
]