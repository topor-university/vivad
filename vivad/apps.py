from django.apps import AppConfig


class VivadConfig(AppConfig):
    name = 'vivad'
