# Generated by Django 2.0.1 on 2018-04-22 19:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('vivad', '0005_auto_20180422_2003'),
    ]

    operations = [
        migrations.CreateModel(
            name='used_usernames',
            fields=[
                ('username', models.CharField(max_length=128, primary_key=True, serialize=False)),
            ],
        ),
    ]
