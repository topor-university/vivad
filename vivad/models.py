from django.conf import settings
from django.db import models

class used_usernames(models.Model):
    username = models.CharField(primary_key=True, max_length=128)
