import json

from django.views import View
from django.http import JsonResponse

from . import models


class UsernameExistJsonView(View):
    def get(self, request):
        return self.get_response(request.GET.get('username'))
      
    def post(self, request):
        return self.get_response(request.POST.get('username'))

    def get_response(self, username):
        if not username:
            return JsonResponse({'error': 'bad request'}, status=400)
        
        exists = models.used_usernames.objects.filter(username=username)\
            .exists()

        return JsonResponse({'username_exist': exists})


class UpdateUserSecrets(View):
    def post(self, request):

        if not request.user.is_authenticated:
            return JsonResponse({'error': 'not authorized'}, status=401)

        response_bad_request = JsonResponse({'error': 'bad request'}, status=400)

        data = request.POST.get('data')
        if not data:
            return response_bad_request

        # data come as string
        # data = json.loads(data)
        # check data is valid json
        try:
            json.loads(data)
        except ValueError:
            return response_bad_request
        request.user.secrets = data
        request.user.save()

        return JsonResponse({'success': True})
