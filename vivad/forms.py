from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import gettext as _

from . import models


class StringifyError:
    def str_error(self):
        def f(e):
            k,v = e
            return '%s: %s' % (k, ','.join(v))
        if '__all__' in self.cleaned_data:
            self.cleaned_data[''] = self.cleaned_data['__all__']
            del self.cleaned_data['__all__']
        return '\n'.join(map(f, self.errors.items()))


class LoginForm(forms.Form, StringifyError):
    prefix = 'login'
    username = forms.CharField(min_length=3, max_length=128, widget=forms.TextInput(attrs={'placeholder':'Логин'}))
    password = forms.CharField(min_length=3, max_length=128, widget=forms.PasswordInput(attrs={'placeholder':'Пароль'}))

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if not get_user_model().objects.filter(username=username).exists():
            raise forms.ValidationError(_('Username does not exists'), code='not-exists')
        return username


class RegistrationForm(LoginForm):
    prefix = 'registration'
    repeat_password = forms.CharField(min_length=3, max_length=128, widget=forms.PasswordInput(attrs={'placeholder':'Подтвердите пароль'}))
    email = forms.EmailField(max_length=128, widget=forms.EmailInput(attrs={'placeholder':'Почта', 'pattern':'^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'}))
    favorite_phrase = forms.CharField(max_length=128, widget=forms.TextInput(attrs={'placeholder':'Любимая фраза'}))
    username_hash = forms.CharField(widget=forms.HiddenInput, required=False)

    def clean_username(self):
        return self.cleaned_data.get('username')

    def clean(self):
        super().clean()
        username_hash = self.cleaned_data['username_hash']
        if models.used_usernames.objects.filter(username=username_hash).exists():
            raise forms.ValidationError(_('Username already in use'), code='exists')

        if self.cleaned_data['password'] != self.cleaned_data['repeat_password']:
            raise forms.ValidationError(_('Passwords do not match'), code='not-match')
        return self.cleaned_data


class AddSecretForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    prefix='secret'
    site_name = forms.CharField(max_length=64, widget=forms.TextInput(attrs={'placeholder':'Название'}))
    url = forms.URLField(min_length=3, max_length=64, widget=forms.TextInput(attrs={'placeholder':'Ссылка'}), required=False)
    username = forms.CharField(max_length=64, widget=forms.TextInput(attrs={'placeholder':'Логин'}), required=False)
    password = forms.CharField(max_length=128, widget=forms.TextInput(attrs={'placeholder':'Пароль'}))
    comment = forms.CharField(max_length=300, widget=forms.Textarea(attrs={'placeholder':'Примечание', 'rows':5, 'cols':36}), required=False)
    directory_id = forms.CharField(max_length=64, widget=forms.HiddenInput, initial=0)


class AddDirectoryForm(forms.Form):
    prefix='directory'
    name = forms.CharField(max_length=64, widget=forms.TextInput(attrs={'placeholder':'Название'}))


class EditPasswordForm(forms.Form, StringifyError):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    prefix='edit_password'
    username = forms.CharField(min_length=3, max_length=128, widget=forms.TextInput(attrs={'placeholder':'Логин'}))
    old_password = forms.CharField(min_length=3, max_length=128, widget=forms.PasswordInput(attrs={'placeholder':'Старый пароль'}))
    new_password = forms.CharField(min_length=3, max_length=128, widget=forms.PasswordInput(attrs={'placeholder':'Новый пароль'}))
    new_repeat_password = forms.CharField(min_length=3, max_length=128, widget=forms.PasswordInput(attrs={'placeholder':'Подтвердите новый пароль'}))

    def clean_old_password(self):
        old_password = self.cleaned_data['old_password']
        if not self.user.check_password(old_password):
            raise forms.ValidationError(_('Wrong old password'), code='wrong')
        return old_password

    def clean(self):
        super().clean()
        if self.cleaned_data['new_password'] != self.cleaned_data['new_repeat_password']:
            raise forms.ValidationError(_('Passwords do not match'), code='not-match')
        return self.cleaned_data

    def save(self):
        if not self.is_valid():
            return False
        self.user.username = self.cleaned_data['username']
        self.user.set_password(self.cleaned_data['new_password'])
        self.user.save()
        return True


class EditFavoritePhraseForm(forms.Form):
    prefix = 'edit_favorite_phrase'
    new_favorite_phrase = forms.CharField(max_length=128, widget=forms.TextInput(attrs={'placeholder':'Новая любимая фраза'}))

    def save(self, user):
        if not self.is_valid():
            return False
        user.favorite_phrase = self.cleaned_data['new_favorite_phrase']
        user.save()
        return True


class DeleteAccountForm(LoginForm):
    prefix = 'delete_account'
    username_hash = forms.CharField(widget=forms.HiddenInput)

    def save(self):
        if not self.is_valid():
            return False

        u_hash = self.cleaned_data['username_hash']
        models.used_usernames.objects.get(username=u_hash).delete()

        u = self.cleaned_data['username']
        get_user_model().objects.get(username=u).delete()
        return True
