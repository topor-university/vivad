import json

from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import View

from . import storage
from . import forms
from . import models


def get_salts():
    return json.dumps(['42'])


class IndexView(View):
    def get(self, request):
        return render(request, 'vivad/index.html')


class IndexFrameView(View):
    def get(self, request):
        if request.user.is_authenticated:
            return render(request, 'vivad/storage.html',
                {'secret_form': forms.AddSecretForm,
                 'directory_form': forms.AddDirectoryForm,
                 'user_data': request.user.secrets
                 })
        else:
            return render(request, 'vivad/home.html')

    def post(self, request):
        if not self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))
        return HttpResponseRedirect(reverse('main-page'))


class LoginView(View):
    def get(self, request):
        if (request.user.is_authenticated):
            return HttpResponseRedirect(reverse('main-page'))

        login_error = request.GET.get('login_error')
        reg_error = request.GET.get('reg_error')

        return render(request, 'vivad/login_register.html',
            {"login_form": forms.LoginForm, "registration_form": forms.RegistrationForm, 'login_error': login_error, 'reg_error': reg_error, 'salts': get_salts()})

    def post(self, request):
        form = forms.LoginForm(request.POST)

        if not form.is_valid():
            return HttpResponseRedirect(reverse('login') + '?login_error=' + form.str_error())

        user = authenticate(request, **form.cleaned_data)
        if user is not None:
            login(request, user)

            return self.set_favorite_phrase_cookie(
                HttpResponseRedirect(reverse('main-page')),
                request.user
            )
        else:
            return HttpResponseRedirect(reverse('login') + '?login_error=' + 'bad login/password')

    @staticmethod
    def set_favorite_phrase_cookie(response, user):
        response.set_cookie('fav', user.favorite_phrase_base64)
        return response


def logoutView(request):
    logout(request)
    return HttpResponseRedirect(reverse('main-page'))


class SettingsView(View):
    def get(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))

        form_result = kwargs.get('form_result')

        return render(request, 'vivad/profile_settings.html',
            {'edit_password_form': forms.EditPasswordForm,
             'edit_favorite_phrase_form': forms.EditFavoritePhraseForm(
                initial={'new_favorite_phrase':request.user.favorite_phrase}),
             'delete_account_form': forms.DeleteAccountForm,
             'form_result': form_result, 'salts': get_salts()
             })

    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))
        return HttpResponseRedirect(reverse('settings'))


class SettingsEditPasswordView(View):
    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))

        user = request.user
        form = forms.EditPasswordForm(request.POST, user=user)
        if form.is_valid():
            form.save()
        else:
            kwargs['form_result'] = form.str_error()
            return SettingsView.get(self, request, *args, **kwargs)  # do not replace old->new
        login(request, user)
        return HttpResponseRedirect(reverse('main-page'))  # do replace old->new key


class SettingsEditFavoritePhraseView(View):
    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))

        form = forms.EditFavoritePhraseForm(request.POST)

        if form.is_valid():
            form.save(request.user)

        return LoginView.set_favorite_phrase_cookie(
            HttpResponseRedirect(reverse('settings')),
            request.user)


class SettingsDeleteAccountView(View):
    def post(self, request, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return HttpResponseRedirect(reverse('login'))

        form = forms.DeleteAccountForm(request.POST)
        print('is_form valid?')
        if form.is_valid():
            print(request.user.username)
            print('yeah oO')
            print(form.cleaned_data)
            form.save()
        else:
            kwargs['form_result'] = 'логин или пароль неверны'
            return SettingsView.get(self, request, *args, **kwargs)

        logout(request)
        response = HttpResponseRedirect(reverse('main-page'))
        response.delete_cookie('fav')
        return response


class RegistrationView(View):
    def post(self, request, *args, **kwargs):
        form = forms.RegistrationForm(request.POST)

        if not form.is_valid():
            return HttpResponseRedirect(reverse('login') + '?reg_error=' +\
                ';'.join(map(lambda v: ','.join(v[1]), form.errors.items())) + '#register')

        models.used_usernames(form.cleaned_data['username_hash']).save()

        del form.cleaned_data['repeat_password']
        del form.cleaned_data['username_hash']

        user = get_user_model().objects.create_user(**form.cleaned_data)

        login(request, user)
        return LoginView.set_favorite_phrase_cookie(
            HttpResponseRedirect(reverse('main-page')),
            request.user
        )
