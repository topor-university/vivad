from django.urls import path

from . import ajax_views

urlpatterns = [
    path('is_username_exist/', ajax_views.UsernameExistJsonView.as_view(), name='ajax_is_username_exist'),
    path('update_user_secrets/', ajax_views.UpdateUserSecrets.as_view(), name='ajax_update_user_secrets'),
]