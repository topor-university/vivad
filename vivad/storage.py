def get_passwords(user):
    return [user.old_secrets.filter(directory_id=None), user.dirs.all()]

def save_secret(user, **kwargs):
    s = user.old_secrets.create(**kwargs)
    s.save()

def save_directory(user, parent_id=None, **kwargs):
    parent = None
    if parent_id:
        kwargs['parent'] = user.dirs.filter(pk=parent_id).first()
    d = user.dirs.create(**kwargs)
    d.save()