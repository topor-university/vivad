import base64
from django import template
from django.template.defaultfilters import stringfilter
register = template.Library()

@register.filter
@stringfilter
def base64_decode(val):
    return base64.decodebytes(val.encode('utf-8')).decode('utf-8')