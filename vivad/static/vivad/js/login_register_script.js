(function() {
	
	login_form = document.getElementById("login_form");
	register_form = document.getElementById("register_form");

	/*Go to registration form*/
	function hashChangeHandler(e) {
		switch (window.location.hash) {
			case '#register':
				login_form.setAttribute('style', 'display:none');
				register_form.setAttribute('style','display:block');
				break;
			case '':
				login_form.setAttribute('style', 'display:block');
				register_form.setAttribute('style','display:none');
				break;
			default: break;
		}
	};
	
	addEventListener('hashchange', hashChangeHandler);	
	addEventListener('load', hashChangeHandler);	


	link_register.onclick=function functionRegistry(){
		document.location.hash='register';
		login_form.setAttribute('style', 'display:none');
		register_form.setAttribute('style','display:block');
	}
	
	/*Cancel button register forms*/
	btn_register_cancel.onclick=function functionCancelRegistration(){
		document.location.hash='';
		login_form.setAttribute('style', 'display:block');
		register_form.setAttribute('style','display:none');
	}

	/*Check login field*/
	username_register = document.getElementById('id_registration-username');
	username_register_error = document.getElementById('login_reg_error');
	username_hash_register = document.getElementById('id_registration-username_hash');

	username_register.onblur = function(){
		if (username_register.value == '') {
			return;
		}

		var hash = sha3_512(username_register.value);
		username_hash_register.value = hash;

		function handler_func(httpRequest) {
			if (httpRequest.status === 200) {
				var obj = JSON.parse(httpRequest.responseText);
				if (obj['username_exist']){
					username_register_error.innerHTML = "Логин занят";
					username_register.setAttribute('style', 'border-color:red');
					username_register.value="";
				}else{
					username_register_error.innerHTML = "";
					username_register.setAttribute('style', 'border-color:none');
				}
			} else {
				console.log('There was a problem with the request.');
			}
		}
		ajax(handler_func, '/ajax/is_username_exist/?username=' + hash, 'GET');
	}

	username_register.onfocus = function(){
		username_register_error.innerHTML = "";
		username_register.setAttribute('style', 'border-color:none');
	}

	/*Check password field*/
	password_register = document.getElementById('id_registration-password');
	repeat_password_register = document.getElementById('id_registration-repeat_password');
	repeat_password_register_error = document.getElementById('password_reg_error');
	repeat_password_register.onblur = function(){
		value_password_register = password_register.value;
		value_repeat_password_register = this.value;
		if (value_repeat_password_register == value_password_register){
			repeat_password_register_error.innerHTML = "";
			password_register.setAttribute('style', 'border-color:none');
			repeat_password_register.setAttribute('style', 'border-color:none');
		}else{
			repeat_password_register_error.innerHTML = "Пароли не совпадают!";
			password_register.setAttribute('style', 'border-color:red');
			repeat_password_register.setAttribute('style', 'border-color:red');
			repeat_password_register.value="";
			password_register.value="";

		}
	}
	
	password_register.onfocus = function(){
		repeat_password_register_error.innerHTML = "";
		password_register.setAttribute('style', 'border-color:none');
		repeat_password_register.setAttribute('style', 'border-color:none');
	}

	/*Exit on esc*/
	login_register_forms = Array.from(document.getElementsByClassName('login-register-box'));
	login_register_forms.forEach(function(form){
		childrens = Array.from(form.children);

		childrens.forEach(function(child){
			addEventListener("keydown", function(event) {
				if (child.valueOf().id=='register_form' && register_form.getAttribute('style')=='display:block' && event.which == 27) {
					btn_register_cancel.click();
					document.location.hash='';
				}
			});
		});
	});

	register_form.addEventListener('submit', function(e){
		var u = username_register.value;
		var p = password_register.value;

		var res = hash_all(u,p);
		var new_username = res[0];
		var new_password = res[1];
		var key = res[2];
		parent_set_item('username', new_username);
		parent_set_item('key', key);

		username_register.value = new_username;
		password_register.value = new_password;
		repeat_password_register.value = new_password;
	});


	username_login = document.getElementById('id_login-username');
	password_login = document.getElementById('id_login-password');

	login_form.addEventListener('submit', function(e){
		var u = username_login.value;
		var p = password_login.value;

		var res = hash_all(u,p);
		var new_username = res[0];
		var new_password = res[1];
		var key = res[2];

		parent_set_item('username', new_username);
		parent_set_item('key', key);

		username_login.value = new_username;
		password_login.value = new_password;
	});
})();
