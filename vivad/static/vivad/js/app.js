(function (){
	
	/* чтобы не разворачивались серверным шаблонизатором
	 * запрещены:
	 * {{ }} // используются по-умолчанию
	 * {% %}
	 * {# #}
	*/

	const delimiters = ['${', '}']; 	
		
	const password_component = {
		delimiters: delimiters,
		props: {
			pass: {},
			current_dir: {},
		},
		methods: {
			show_site_info_modal: function(){
				this.$root.bus.$emit(
					'set_current_dir_pass_name_and_call_site_info_popup',
					this.current_dir, this.pass);
			},
			deleteSiteFromList: function(){
				var isDelete = confirm("Вы точно хотите удалить сайт '" + this.pass.name + "'?");
				if (!isDelete){
					return;
				}
				var index = this.$parent.dir.childs.indexOf(this.pass);
				this.$parent.dir.childs.splice(index, 1);
				this.$root.bus.$emit('send_data_to_server');
			},
		},
		template: '#password-template',
	};

	
	const directory_component = {
		delimiters: delimiters,
		props: {
			dir:{},
			is_expandable: {default: true}, /* можно ли раскрыть */
			init_is_expand: {default: false},
			init_is_only_dir: {default: false}, /* отображать только директории */
			display_header: {default: true},
			display_back_button: {default: true},
		},
		data: function(){
			return {
				is_expand: this.init_is_expand,
				is_only_dir: this.init_is_only_dir,
				show_directory_modal: false,
			};
		},
		computed: {
			is_child_directories_exists: function(){
				return this.dir.childs.filter(function(e){return e.type == 'd';}).length > 0;
			},
			directories: function(){
				return this.dir.childs.filter(function(e){ return e.type == 'd'; });
			},
			passwords: function(){
				return this.dir.childs.filter(function(e){ return e.type == 'p'; });
			},
		},
		methods: {
			toggle_expand: function(e){
				if (this.is_expandable){
					this.is_expand = !this.is_expand;
				}
			},
			display_this_dir: function(e){
				this.$root.bus.$emit('set_displayed_dir', this.dir);
			},
			display_parent: function(){
				this.$root.bus.$emit('set_displayed_dir', this.dir._parent());
			},
			show_add_directory_modal: function(dir){
				this.$root.bus.$emit('set_current_dir_and_call_directory_popup', dir);
			},
			show_add_site_modal: function(dir){
				this.$root.bus.$emit('set_current_dir_and_call_site_popup', dir);
			},
			deleteFromList: function(){
				var isDelete = confirm("Вы точно хотите удалить категорию '" + this.dir.name + "' и все ее содержимое?");
				if (!isDelete){
					return;
				}
				
				var index = this.$parent.dir.childs.indexOf(this.dir);
				this.$parent.dir.childs.splice(index, 1);
				this.$root.bus.$emit('send_data_to_server');
			},
		},
		components: {
			directory: null /* this component */,
			password: password_component,
		},
		template: '#directory-template',
	};
	directory_component.components['directory'] = directory_component
	
	const modal_directory_component = {
		data: function(){
			return {
				directory_name: '',
			};
		},
		props: {
			curr_dir:{},				
		},
		methods: {
			handle_button_ok: function(e){
				e.preventDefault();
				var that = this;
/*??? но почему выводится кракозябра, а не 213	
Потому что механизм vue такой. При каждом изменении поля - внутрь вью отправляются изменения и дергается соответствующая функция чтобы перерисовать дерево элементов HTML
реактивный - то есть он ... ну в общем он содержит в себе не обычное значение а дополнительно проворачивает некоторые манипуляции)
Для "нормального" вывода можно использовать например...
*/
				this.curr_dir.childs.push({
					type: 'd',
					name:this.directory_name, 
					childs: [],
					_parent: function(){ return that.curr_dir; } //здесь нужна магия замыканий, иначе this - указывает на 'функцию', а нужно, чтобы указывал на 'компонент'
				});
				this.$emit('close');
				this.$root.bus.$emit('send_data_to_server');
				e.preventDefault();
			},
		},
		template: '#modal-new-directory-template',
	};
	
	const modal_generate_component = {
		data: function(){	
			if(this.size){
				return {
					password: this.value,
					size: this.size,
					checkedAlphabet:[],
				};
			}		
			else {
				return {
					password: this.value,
					size: 8,
					checkedAlphabet:[],
				};
			}
		},
		props: {
			auto: [String, Boolean],			
		},
		mounted: function() {
			if(this.auto == 'true' || this.auto == 1) {
				this.checkedAlphabet.push('ABZ');
				this.checkedAlphabet.push('abz');
				this.checkedAlphabet.push('019');
				this.checkedAlphabet.push('!$^');
				if(this.checkedAlphabet != '') {
					this.generate_password(full_alphabet);
				}
			}
		},
		methods: {
			generate_password: function(checkedAlphabet){
				let CharacterSet = '';
				if(this.checkedAlphabet != '') {
					if( checkedAlphabet.indexOf('ABZ') >= 0) {
						CharacterSet += get_alphabet('A','Z');
					}
					if( checkedAlphabet.indexOf('abz') >= 0) {
						CharacterSet += get_alphabet();
					}
					if( checkedAlphabet.indexOf('019') >= 0) {
						CharacterSet += get_alphabet('0','9');
					}
					if( checkedAlphabet.indexOf('!$^') >= 0) {
						CharacterSet += '![]{}()%&*$#^<>~@|';
					}
					if( checkedAlphabet.indexOf('ABZ') <0 && checkedAlphabet.indexOf('abz') <0 && checkedAlphabet.indexOf('019') <0 && checkedAlphabet.indexOf('!$^') <0 ) {
						CharacterSet = full_alphabet;
					}
				}

				if (this.size < 3) { this.size = 5; }
				passwd = '';
				for (let i=0; i<this.size; i++){
					passwd += CharacterSet.charAt(Math.floor(Math.random() * CharacterSet.length));
				}
				this.password = passwd;
			},
		},
		template: '#modal-generate-template',
	};
	
	const modal_site_component = {
		data: function(){
			if (this.site){
				return {
					site_name: this.site.name,
					site_url: this.site.url,
					site_username: this.site.username,
					site_password: this.site.password,
					site_comment: this.site.comment,
					visible_generate:false,
				};
			}
			
			return {
				visible_generate:false,
				site_name: '',
				site_url: '',
				site_username: '',
				site_password: '',
				site_comment: '',
			};
		},
		props: {
			curr_dir:{},				
			site:{ default:false },				
		},
		methods: {
			handle_button_ok: function(e){
				e.preventDefault();
				var that = this;
				if (this.site){
					// мы изменяем уже созданный сайт
					this.site.name = this.site_name;
					this.site.username = this.site_username;
					this.site.password = this.site_password;
					this.site.url = this.site_url;
					this.site.comment = this.site_comment;
				}else{
					// мы добавляем сайт
					this.curr_dir.childs.push({
						type: 'p',
						name: this.site_name,
						username: this.site_username,
						password: this.site_password,
						url: this.site_url,
						comment: this.site_comment,
					});
				}
				this.$emit('close');
				this.$root.bus.$emit('send_data_to_server');
				e.preventDefault();
			},
			encrypt_field: function(e){
				if (e.target.value == '')
					return;
				e.target.value = encrypt(e.target.value);
				/* магия с оповещением vue */
				e.target.dispatchEvent(new Event('input', { 'bubbles': true }))
			},
			decrypt_field: function(e){
				var decrypt_value = decrypt(e.target.value);
				if (decrypt_value == '')
					return;
				e.target.value = decrypt_value;
				/* магия с оповещением vue */
				e.target.dispatchEvent(new Event('input', { 'bubbles': true }))
			},
		},
		components: {
			modal_generate: modal_generate_component,
		},
		template: '#modal-site-template',
	};
	
	function set_parents(struct, parent){
		
		struct._parent = function(){
			return parent;
		};
		
		for(var i=0;i<struct.childs.length;i++){
			if (struct.childs[i].type == 'd'){
				set_parents(struct.childs[i], struct);
			}
		}
	}
	
	var root_directory = document.memory['user_data'];
	
	window.onload = function() {
		set_parents(root_directory, root_directory);
		
		document.vue = new Vue({
			el: '#storage_content',
			delimiters: delimiters,
			data: {			
				fullTree: root_directory,
				
				displayed_dir: root_directory,
				
				show_directory_modal: false,
				show_site_modal: false,
				
				current_dir: root_directory,				
				current_site: false,
				search_text: '',
				
				bus: new Vue(),
			},
			created: function(){
				that = this; /* магия замыканий */
				this.bus.$on('set_displayed_dir', function(dir){
					/*
					 * dir should have: name, childs
					*/
					that.displayed_dir = dir;
				});
				
				this.bus.$on('set_current_dir_and_call_directory_popup', function(dir){
					that.current_dir = dir;
					that.show_directory_modal = true;
				});
				
				this.bus.$on('set_current_dir_and_call_site_popup', function(dir){
					that.current_dir = dir;
					that.current_site = false;
					that.show_site_modal = true;
				});

				this.bus.$on('set_current_dir_pass_name_and_call_site_info_popup', function(dir, site){
					that.current_dir = dir;
					that.current_site = site;
					that.show_site_modal = true;
				});
				
				this.bus.$on('send_data_to_server', function(){
					document.memory['user_data'] = that.fullTree
					send_user_data();
					//prompt('server data ');
				});				
			},
			methods: {
				show_add_directory_modal: function(dir){
					this.$root.bus.$emit('set_current_dir_and_call_directory_popup', dir);
				},				
				show_add_site_modal: function(dir){
					this.$root.bus.$emit('set_current_dir_and_call_site_popup', dir);
				},
				force_update_tree: function(){
					this.$set(this.fullTree, 'childs', document.memory['user_data'].childs);
					set_parents(this.fullTree, this.fullTree);
					send_user_data();
				},
				search: function(e){
					e.preventDefault();
					var ft = this.fullTree;
					var dir = {type: 'd', name: 'результаты поиска',
						_parent: function(){ return ft; },
						childs: []};

					dir['childs'].push()
					var q = this.search_text.toLowerCase();
					console.log('here');

					function local_search(tree){
						tree.forEach(function(v) {
							if (v.name.toLowerCase().indexOf(q) > -1){
								dir['childs'].push(v);
							}
							if (v.type == 'd'){
								local_search(v.childs);
							}
						});
					}
					local_search(this.fullTree.childs);
					this.displayed_dir = dir;
				},
			},
			components: {
				directory: directory_component,
				password: password_component,
				modal_directory: modal_directory_component,
				modal_site: modal_site_component,
			},
			template : '#main-template',
		});
	}
})()