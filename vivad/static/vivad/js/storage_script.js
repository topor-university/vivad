id_secret_directory = document.getElementById('id_secret-directory_id');

generate_password_form = document.getElementById('block_generate_password');

/*Переменные для формы генерации пароля*/
div_site_form_password_form = document.getElementById('with-or-without-generate-password-form');
col_site_form_password_form = document.getElementById('col-with-or-without-generate-password');

popup_forms = Array.from(document.getElementsByClassName('popup_form'));
popup_forms.forEach(function(){
	addEventListener("keydown", function(event) {
		if (generate_password_form.style.display == 'block' && event.which == 27) {			
			generate_password_form.style.display = 'none';
			div_site_form_password_form.classList.remove("with-generate-password-form");
			col_site_form_password_form.classList.remove("col-add-site-generate-password");
		}	
		if (document.location.hash=='#popup_show_site' && event.which == 27) {
			document.location='#';
		}
	});
 });

btn_cancel_add_site.onclick=function funcCancel(){
	id_secret_directory.value = 0;
}

btn_cancel_show_site.onclick=function funcCancel(){
	document.location='#';
}

  if (!Element.prototype.closest) {

    // реализуем
    Element.prototype.closest = function(css) {
      var node = this;

      while (node) {
        if (node.matches(css)) return node;
        else node = node.parentElement;
      }
      return null;
    };
  }
add_site_buttons = Array.from(document.getElementsByClassName('add-site-button'));
show_site_buttons = Array.from(document.getElementsByClassName('show-site-button'));
add_site_buttons.forEach(function(btn){
	btn.addEventListener('click', function(){
		let btn_parent=this.closest('.panel');
		if(btn_parent==null){
			return;
		}
		let id = btn_parent.children[0].value;
		if(id == undefined){
			id=0;
		}
		id_secret_directory.value=id;
	});
});


show_site_buttons.forEach(function(btn){
	btn.addEventListener('click', function(){
		let pk = this.id;
		res = all_passwords.filter(function(e){ return e.pk == pk; });
		if (res.length != 1){
			return;
		}
		res = res[0];

		// TODO: исправить
		// предполагаю, что label будет заменены input'ами, может быть изменен порядок
		fields = ['site_name', 'url', 'username', 'password', 'comment'];
		labels = document.querySelector('#popup_show_site .form_label').children;

		for(let i=1, field_id=0;i<labels.length; i+=3, field_id++){
			labels[i].innerHTML = res.fields[fields[field_id]];
		}
	});
});

/*Сгенерировать пароль*/
btn_generate_password.onclick=function funcHandleClickOnGeneratePassword(){
	div_site_form_password_form.classList.add('with-generate-password-form');
	col_site_form_password_form.classList.add('col-add-site-generate-password');
	generate_password_form.style.display = 'block';
}

/*Кнопка отмены генерации пароля*/
btn_cancel_generate_password.onclick=function funcCancel(){
	generate_password_form.style.display = 'none';
	div_site_form_password_form.classList.remove("with-generate-password-form");
	col_site_form_password_form.classList.remove("col-add-site-generate-password");
}


/* отправка данных пользователя на сервер */
function send_user_data(){
	var data = JSON.stringify(document.memory['user_data'].childs);
	parent_set_item('user_data',document.memory['user_data']);
	function handler_func(httpRequest) {
		if (httpRequest.status === 200) {
			return;
		} else {
			console.log('что-то пошло не так, не удалось сохранить')
			console.log(httpRequest.status);
		}
	}
	ajax(handler_func, '/ajax/update_user_secrets/', 'POST',
		'data=' + (data), csrf_token);
}

/*Get alphabet*/
var full_alphabet = get_alphabet().concat(get_alphabet('A','Z')).concat(get_alphabet('0','9'));
full_alphabet+='![]{}()%&*$#^<>~@|';


function get_alphabet(charStart = 'a', charEnd = 'z'){
	let r = '';
	end = charEnd.charCodeAt(0);
	start = charStart.charCodeAt(0);
	for(let i = start; i <= end; i++){
		r+=String.fromCharCode(i);
	}
	return r;
}


document.addEventListener("DOMContentLoaded", function() {
	parent_set_item('user_data', document.memory['user_data']);

	parent_set_item('do_change', 'true')
	parent_get_item('key');
	parent_get_item('user_data');
});