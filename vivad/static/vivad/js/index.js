function toChild(data){
	window.frames.target.postMessage(data, "http://127.0.0.1:8000/");;
}

document.memory = {};

(function() {

	function get_keys_start_with(dict, start){
		var res = []
		for(var key in dict){
			key = '' + key;
			if (key.startsWith(start)){
				res.push(key);
			}
		}
		return res;
	}

	function listener(event) {
		if (event.origin != "http://127.0.0.1:8000") {
			return;
		}
		var start = 'new_';
		try{
			var res = JSON.parse(event.data);
			if(res['set']){
				var obj = res['set'];

				if (obj['do_change'] == 'true'){
					var keys = get_keys_start_with(document.memory, start);
					for (var i in keys){
						var from = keys[i];
						var to = from.slice(start.length);
						document.memory[to] = document.memory[from];
						delete	document.memory[from];
					}
					return;
				}
				if (obj['do_change'] == 'clear'){
					var keys = get_keys_start_with(document.memory, start);
					for (var i in keys){
						delete document.memory[keys[i]];
					}
					return;
				}
				Object.assign(document.memory, obj);
			}
			if(res['get']){
				var key = res['get'];
				var val = document.memory[key];
				if(val) {
					var obj = {}
					obj[key] = document.memory[key];
					toChild(JSON.stringify(obj));
				}
			}
		}
		catch(e){}
		// alert( "получено: " + event.data );
	}

	window.addEventListener("message", listener);
})();

	