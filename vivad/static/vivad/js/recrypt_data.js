function recrypt(user_data, old_key, new_key){
		
	for(var i = 0; i < user_data.childs.length; i++){
		if(user_data.childs[i].type == 'p'){

			if(user_data.childs[i].url != ''){
				user_data.childs[i].url	= encrypt(decrypt(user_data.childs[i].url, old_key), new_key);
			}
			
			if(user_data.childs[i].username != ''){
				user_data.childs[i].username = encrypt(decrypt(user_data.childs[i].username, old_key), new_key);
			}
			
			if(user_data.childs[i].password != ''){
				user_data.childs[i].password = encrypt(decrypt(user_data.childs[i].password, old_key), new_key);
			}
			
			if(user_data.childs[i].comment != ''){
				user_data.childs[i].comment	= encrypt(decrypt(user_data.childs[i].comment, old_key), new_key);
			}
		} else {
			recrypt(user_data.childs[i], old_key, new_key);
		}
	}	
}