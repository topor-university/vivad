function ajax(handler_func, url, method, data, csrf_token){
	var httpRequest;
	httpRequest = new XMLHttpRequest();

	if (!httpRequest) {
		console.log('Giving up :( Cannot create an XMLHTTP instance');
		return false;
	}
	
	function onReadyStateChange() {
		try {
			if (httpRequest.readyState === XMLHttpRequest.DONE) {
				handler_func(httpRequest);
			}
		}
		catch( e ) {
			console.log('Caught Exception: ' + e.description);
		}
	}
	
	httpRequest.onreadystatechange = onReadyStateChange;
	httpRequest.open(method, url);

	if (method.toLowerCase().trim() == 'post'){	
		httpRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
	}

	if(csrf_token){
		httpRequest.setRequestHeader('X-CSRFToken', csrf_token);
	}
	httpRequest.send(data);
}

