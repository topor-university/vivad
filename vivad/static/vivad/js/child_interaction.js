(function(){
	window.addEventListener("message", function (event) {
		if (event.origin != "http://127.0.0.1:8000") {
			return;
		}
		try{
			var res = JSON.parse(event.data);
			if(document.memory == undefined){
				document.memory = {};
			}
			Object.assign(document.memory, res);

			if(res['user_data'] && document.vue){
				document.vue.force_update_tree();
			}
		}catch(e){}
	});
})();

function toParent(data){
	window.parent.postMessage(data, "http://127.0.0.1:8000");
}
function parent_set_item(key, val){
	var obj = {}
	obj[key] = val;
	toParent(JSON.stringify({'set':obj}));
}
function parent_get_item(key){
	toParent(JSON.stringify({'get':key}));
}
