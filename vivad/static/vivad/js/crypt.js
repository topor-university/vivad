function encrypt(value, key=undefined) {
	if (!key){
		key = document.memory['key'];
	}
	return btoa(CryptoJS.AES.encrypt(value, key));
}

function decrypt(value, key=undefined) {
	if (!key){
		key = document.memory['key'];
	}
	return CryptoJS.AES.decrypt(atob(value), key).toString(CryptoJS.enc.Utf8);
}
