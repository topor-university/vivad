function getRandomInt(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/*
 * generate array of char for given range
 * default start = 'a'
 * default end = 'z'
 * default return = ['a', ..., 'z']
*/
function getAlphabet(charStart = 'a', charEnd = 'z'){
	let r = Array(),
		end = charEnd.charCodeAt(0),
		start = charStart.charCodeAt(0);
	for(let i = start; i < end; i++){
		r.push(String.fromCharCode(i));
	}
	return r;
}

var full_alphabet = getAlphabet().concat(getAlphabet('A','Z')).concat(getAlphabet('0','9'));


/* 
 * generate 1 password for given length and alphabet
 * default length = 8
 * default alphabet = [a-zA-Z0-9]
*/
function genPasswd(len = 8, alphabet) {
	if (len < 0) { len = 8; }
	alphabet = alphabet || full_alphabet;
	len_alphabet = alphabet.length;
	passwd = [];
	for (let i=0; i<len; i++){
		passwd.push(alphabet[getRandomInt(0, alphabet.length)]);
	}
	return passwd.join('');
}