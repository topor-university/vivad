//HASH
function hash_it(username, pass, salt0, salt1, salt2, salt3){
	var temp_val = sha3_512(username + salt0 + pass) +
			sha3_512(username + salt1 + pass);
	var temp_key = sha3_512(
			sha3_512(username + salt2 + pass) +
			sha3_512(username + salt3 + pass)
		);
	var iv = CryptoJS.enc.Utf8.parse(temp_key.substr(0, 32));
	temp_key = CryptoJS.enc.Utf8.parse(temp_key.substr(32, 32));
	return CryptoJS.AES.encrypt(temp_val, temp_key, {iv: iv}).toString().substr(0,64);
}

function hash_all(u, p){
	salt0 = salts[0 % salts.length]
	salt1 = salts[1 % salts.length]
	salt2 = salts[2 % salts.length]
	salt3 = salts[3 % salts.length]
	new_username = hash_it(u, p, salt0, salt1, salt2, salt3);

	salt0 = salts[4 % salts.length]
	salt1 = salts[5 % salts.length]
	salt2 = salts[6 % salts.length]
	salt3 = salts[7 % salts.length]

	new_password = hash_it(u, p, salt0, salt1, salt2, salt3);

	salt0 = salts[8 % salts.length]
	salt1 = salts[9 % salts.length]
	salt2 = salts[10 % salts.length]
	salt3 = salts[11 % salts.length]
	key = hash_it(u, p, salt0, salt1, salt2, salt3);
	return [new_username, new_password, key]
}
