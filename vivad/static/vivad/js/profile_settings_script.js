(function() {
	
	/*esc*/
	
	div_settings_passwords = Array.from(document.getElementsByClassName('settings_password'));
	div_settings_phrases = Array.from(document.getElementsByClassName('settings_phrase'));
	div_settings_deletes = Array.from(document.getElementsByClassName('settings_delete'));
	
	edit_password_form = document.getElementById("settings_password_form");
	edit_favorite_phrase_form = document.getElementById("settings_phrase_form");
	delete_account_form = document.getElementById("settings_delete_form");

	link_edit_password.onclick=function functionEditPassword(){
		edit_favorite_phrase_form.setAttribute('style', 'display:none');
		delete_account_form.setAttribute('style', 'display:none');
		edit_password_form.setAttribute('style','display:block');
		
		div_settings_passwords.forEach(function(form){
			childrens = Array.from(form.children);
		
			childrens.forEach(function(child){
				addEventListener("keydown", function(event) {
					if (child.valueOf().id=='settings_password_form' && 	event.which == 27) {
						btn_cancel_edit_password.click();
					}
				});
			});
		});
	}	
	
	link_edit_favorite_phrase.onclick=function functionEditPhrase(){
		delete_account_form.setAttribute('style', 'display:none');
		edit_password_form.setAttribute('style', 'display:none');
		edit_favorite_phrase_form.setAttribute('style','display:block');
		
		div_settings_phrases.forEach(function(form){
			childrens = Array.from(form.children);
		
			childrens.forEach(function(child){
				addEventListener("keydown", function(event) {
					if (child.valueOf().id=='settings_phrase_form' && 	event.which == 27) {
						btn_cancel_edit_phrase.click();
					}
				});
			});
		});
	}		
	
	link_delete_account.onclick=function functionDeleteAccount(){
		edit_favorite_phrase_form.setAttribute('style', 'display:none');
		edit_password_form.setAttribute('style', 'display:none');
		delete_account_form.setAttribute('style','display:block');
		
		div_settings_deletes.forEach(function(form){
			childrens = Array.from(form.children);
		
			childrens.forEach(function(child){
				addEventListener("keydown", function(event) {
					if (child.valueOf().id=='settings_delete_form' && 	event.which == 27) {
						btn_cancel_delete_account.click();
					}
				});
			});
		});
	}
	
	/*click btn cancel*/

	btn_cancel_edit_password.onclick=function functionCancelEditPassword(){
		edit_password_form.setAttribute('style', 'display:none');
	}
	
	btn_cancel_edit_phrase.onclick=function functionCancelEditPhrase(){
		edit_favorite_phrase_form.setAttribute('style', 'display:none');
	}
	
	btn_cancel_delete_account.onclick=function functionCancelDeleteAccount(){
		delete_account_form.setAttribute('style','display:none');
	}

	/*password verification*/
	var username = document.getElementById('id_edit_password-username');
	var old_password = document.getElementById('id_edit_password-old_password');
	var new_password = document.getElementById('id_edit_password-new_password');
	var new_repeat_password = document.getElementById('id_edit_password-new_repeat_password');
	var repeat_password_edit_error = document.getElementById('edit_password_error');

	new_password.onfocus = function(){
		repeat_password_edit_error.innerHTML = "";
		new_password.setAttribute('style', 'border-color:none');
		new_repeat_password.setAttribute('style', 'border-color:none');
	}

	new_repeat_password.onblur = function(){
		value_password = new_password.value;
		value_repeat_password = this.value;
		if (value_repeat_password == value_password){
			repeat_password_edit_error.innerHTML = "";
			new_password.setAttribute('style', 'border-color:none');
			new_repeat_password.setAttribute('style', 'border-color:none');
		}else{
			repeat_password_edit_error.innerHTML = "Пароли не совпадают!";
			new_password.setAttribute('style', 'border-color:red');
			new_repeat_password.setAttribute('style', 'border-color:red');
			new_password.value="";
			new_repeat_password.value="";
		}
	}

	edit_password_form.addEventListener('submit', function(e){
		var u = username.value;
		var op = old_password.value;
		var np = new_password.value;

		var res_old = hash_all(u, op);
		var res_new = hash_all(u, np);

		old_password.value = res_old[1];

		username.value = res_new[0];
		new_password.value = res_new[1];
		new_repeat_password.value = res_new[1];

		var user_data = document.memory['user_data'];
		recrypt(user_data, res_old[2], res_new[2]);

		parent_set_item('new_username', res_new[0]);
		parent_set_item('new_key', res_new[2]);
		parent_set_item('new_user_data', user_data);
	});

	document.addEventListener("DOMContentLoaded", function() {
		parent_get_item('user_data');
		parent_set_item('do_change', 'clear');
	});


	/* delete account*/
	var del_username = document.getElementById('id_delete_account-username');
	var del_password = document.getElementById('id_delete_account-password');
	var del_username_hash = document.getElementById('id_delete_account-username_hash');
	delete_account_form.addEventListener('submit', function(e){
		var u = del_username.value;
		var p = del_password.value;

		var res = hash_all(u,p);
		del_username.value = res[0];
		del_password.value = res[1];
		del_username_hash.value = sha3_512(u);
	});

})();